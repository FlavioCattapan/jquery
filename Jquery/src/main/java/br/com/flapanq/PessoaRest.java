package br.com.flapanq;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

@Path("pessoa")
public class PessoaRest {

	@Path("p")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String listarPessoas() {
		List<Pessoa> pessoas = new ArrayList<Pessoa>();
		
		Pessoa pessoa = new Pessoa();
		pessoa.setNome("Fl�vio");
		pessoa.setCpf("124453");
		pessoa.setSexo("M");
		
		pessoas.add(pessoa);
		
		pessoa = new Pessoa();
		pessoa.setNome("Marcos");
		pessoa.setCpf("124453");
		pessoa.setSexo("M");
		
		pessoas.add(pessoa);
		
		Gson gson = new Gson();
		
		String res = gson.toJson(pessoas);
		System.out.println(res);
		
		return res;
	}
	
}
