package br.com.flapanq.objetos;


public interface Entidade {
	
	public int getId();
	
	public String getDescricao();

}
